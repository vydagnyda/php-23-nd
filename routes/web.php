<?php
ini_set('display_errors', 1);
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/hello', function () {
    echo 'Labas pasauli';
})->name('hello');

Route::get('/hello/{name}', function ($name) {
    echo "Labas, $name";
})->name('hello_name');

Route::get('/skaiciuokle', function () {
    return view('skaiciuokle');
});

Route::post('/suma', 'SkaiciuokleController@suma')->name('suma');
Route::get('/kmi', 'KMIController@kmi')->name('kmi');
Route::post('/kmi/result', 'KMIController@result')->name('kmi_result');

Route::get('/owner', 'OwnerController@list')->name('owner_list');

Route::get('/comments', 'CommentsController@list')->name('comment_list');
Route::get('/cars', 'CarsController@list')->name('cars_list');