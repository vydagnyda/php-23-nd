<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Comment;

class SkaiciuokleController extends Controller
{
    public function suma(Request $request)
    {
  
        $a = $request->input('a');
        $b = $request->input('b');

        return view('suma', [
            'a' => $a,
            'b' => $b,
            'sum' => $a + $b
        ]);
    }
}