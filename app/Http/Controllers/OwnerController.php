<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Owner;

class OwnerController extends Controller
{
    public function list(Request $request)
    {
        return view('owner_list', ['owners' => Owner::all()]);
    }
}

  

