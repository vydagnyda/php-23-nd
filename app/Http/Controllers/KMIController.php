<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class KMIController extends Controller
{
    public function kmi(Request $request)
    {
        return view('kmi');
    }

    public function result(Request $request)
    {
        $h = $request->input('height');
        $w = $request->input('weight');
        return view('kmi_result', [
            'height' => $h,
            'weight' => $w,
            'kmi' => $w / ($h * $h),
        ]);
    }
}
